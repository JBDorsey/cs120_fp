#ifndef MOBILEBATTLESHIP_H
#define MOBILEBATTLESHIP_H
/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * Final Project
 * 4/29/16
 * battleship.h
 */

#include "game.h"
#include "battleship.h"

class MobileBattleshipGame : public BattleshipGame {

public:
	MobileBattleshipGame();
	MobileBattleshipGame(bool test) : BattleshipGame(test){}
	//virtual ~MobileBattleshipGame(){};
	virtual GameResult moveShip(string str);

private:
	int findShip(Coord coord);
	GameResult callMove(int loc, int dist, char dir);
	GameResult moveUp1(int loc, int dist);
	GameResult moveUp2(int loc, int dist);
	GameResult moveDown1(int loc, int dist);
	GameResult moveDown2(int loc, int dist);
	GameResult moveLeft1(int loc, int dist);
	GameResult moveLeft2(int loc, int dist);
	GameResult moveRight1(int loc, int dist);
	GameResult moveRight2(int loc, int dist);
};



#endif
