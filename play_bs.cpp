#include "battleship.h"
#include "mobilebattleship.h"
#include <iostream>

/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * Final Project
 * 4/29/16
 * play_bs.cpp
 */

using namespace std;

void game1() {
	cout << "ENTER GAME 1\n" << endl;


	BattleshipGame bsg(true);

	int a = 0;
	assert(bsg.makeShips1("00v", a)); //vert AC at 0,0
	a++;
	assert(bsg.makeShips1("43v", a)); //vert B at 4,3
	a++;
	assert(!bsg.makeShips1("*0v", a)); //invalid
	assert(!bsg.makeShips1("3^h", a)); //invlaid
	assert(!bsg.makeShips1("98v", a)); //oob
	assert(bsg.makeShips1("97v", a)); //vert C at 9,7
	a++;
	assert(!bsg.makeShips1("24h", a)); //intersects
	assert(!bsg.makeShips1("80h", a)); //oob
	assert(bsg.makeShips1("70h", a)); //hori S at 7,0
	a++;
	assert(bsg.makeShips1("09h", a)); //hori D at 0,9


	a = 0;
	assert(bsg.makeShips2("00h", a)); //hori AC at 0,0
	a++;
	assert(bsg.makeShips2("34h", a)); //hori B at 3,4
	a++;
	assert(!bsg.makeShips2("*0v", a)); //invalid
	assert(!bsg.makeShips2("3^h", a)); //invlaid
	assert(!bsg.makeShips2("89h", a)); //oob
	assert(bsg.makeShips2("79h", a)); //hori C at 7,9
	a++;
	assert(!bsg.makeShips2("42v", a)); //intersects
	assert(!bsg.makeShips2("08v", a)); //oob
	assert(bsg.makeShips2("07v", a)); //vert S at 0,7
	a++;
	assert(bsg.makeShips2("90v", a)); //vert D at 9,0


	assert(bsg.getP1Ships().size() == 5);
	assert(bsg.getP2Ships().size() == 5);


	assert(bsg.getP2Ships().at(0).locations.size() == 5);
	assert(bsg.getP2Ships().at(0).hits.size() == 0);
	GameResult result = bsg.attack_square(make_pair(0, 1)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP2Ships().at(0).locations.size() == 4);
	assert(bsg.getP2Ships().at(0).hits.size() == 1);


	assert(bsg.getP1Ships().at(0).locations.size() == 5);
	assert(bsg.getP1Ships().at(0).hits.size() == 0);
	result = bsg.attack_square(make_pair(1, 0)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP1Ships().at(0).locations.size() == 4);
	assert(bsg.getP1Ships().at(0).hits.size() == 1);


	result = bsg.attack_square(make_pair(0, 1)); // P1 HIT SAME
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP2Ships().at(0).locations.size() == 4);
	assert(bsg.getP2Ships().at(0).hits.size() == 1);


	result = bsg.attack_square(make_pair(1, 0)); // P2 HIT SAME
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP1Ships().at(0).locations.size() == 4);
	assert(bsg.getP1Ships().at(0).hits.size() == 1);


	result = bsg.attack_square(make_pair(5, 5)); // P1 MISS
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP2Ships().at(0).locations.size() == 4);
	assert(bsg.getP2Ships().at(0).hits.size() == 1);


	result = bsg.attack_square(make_pair(5, 5)); // P2 MISS
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP1Ships().at(0).locations.size() == 4);
	assert(bsg.getP1Ships().at(0).hits.size() == 1);


	result = bsg.attack_square(make_pair(0, 10)); // oob
	assert(result == RESULT_INVALID);


	result = bsg.attack_square(make_pair(10, 0)); // oob
	assert(result == RESULT_INVALID);


	result = bsg.attack_square(make_pair(-1, 0)); // oob
	assert(result == RESULT_INVALID);


	result = bsg.attack_square(make_pair(0, -1)); // oob
	assert(result == RESULT_INVALID);


	assert(bsg.getP2Ships().at(4).locations.size() == 2);
	assert(bsg.getP2Ships().at(4).hits.size() == 0);
	result = bsg.attack_square(make_pair(1, 9)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP2Ships().at(4).locations.size() == 1);
	assert(bsg.getP2Ships().at(4).hits.size() == 1);


	assert(bsg.getP1Ships().at(4).locations.size() == 2);
	assert(bsg.getP1Ships().at(4).hits.size() == 0);
	result = bsg.attack_square(make_pair(9, 1)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP1Ships().at(4).locations.size() == 1);
	assert(bsg.getP1Ships().at(4).hits.size() == 1);


	result = bsg.attack_square(make_pair(0, 9)); // P1 HIT, SUNK D
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP2Ships().size() == 4);


	result = bsg.attack_square(make_pair(9, 0)); // P2 HIT, SUNK D
	assert(result == RESULT_KEEP_PLAYING);
	assert(bsg.getP1Ships().size() == 4);


	//quickly erases three ships
	for (int b = 0; b < 3; b++) {
		bsg.p1Ships.erase(bsg.p1Ships.begin());
	}


	assert(bsg.getP1Ships().size() == 1);
	bsg.setCurr(bsg.getP2());
	result = bsg.attack_square(make_pair(0, 9)); // HIT
	assert(result == RESULT_KEEP_PLAYING);
	bsg.setCurr(bsg.getP2());
	result = bsg.attack_square(make_pair(0, 7)); // HIT
	assert(result == RESULT_KEEP_PLAYING);
	bsg.setCurr(bsg.getP2());
	result = bsg.attack_square(make_pair(0, 8)); // P2 WINS
	assert(result == RESULT_PLAYER2_WINS);

	cout << "\nPASSED GAME 1\n" << endl;
}



void game2() {
	cout << "ENTER GAME 2\n" << endl;

	MobileBattleshipGame mbsg = MobileBattleshipGame(true);

	int a = 0;
	assert(mbsg.makeShips1("00v", a)); //vert AC at 0,0
	a++;
	assert(mbsg.makeShips1("43v", a)); //vert B at 4,3
	a++;
	assert(!mbsg.makeShips1("*0v", a)); //invalid
	assert(!mbsg.makeShips1("3^h", a)); //invlaid
	assert(!mbsg.makeShips1("98v", a)); //oob
	assert(mbsg.makeShips1("97v", a)); //vert C at 9,7
	a++;
	assert(!mbsg.makeShips1("24h", a)); //intersects
	assert(!mbsg.makeShips1("80h", a)); //oob
	assert(mbsg.makeShips1("70h", a)); //hori S at 7,0
	a++;
	assert(mbsg.makeShips1("09h", a)); //hori D at 0,9


	a = 0;
	assert(mbsg.makeShips2("00h", a)); //hori AC at 0,0
	a++;
	assert(mbsg.makeShips2("34h", a)); //hori B at 3,4
	a++;
	assert(!mbsg.makeShips2("*0v", a)); //invalid
	assert(!mbsg.makeShips2("3^h", a)); //invlaid
	assert(!mbsg.makeShips2("89h", a)); //oob
	assert(mbsg.makeShips2("79h", a)); //hori C at 7,9
	a++;
	assert(!mbsg.makeShips2("42v", a)); //intersects
	assert(!mbsg.makeShips2("08v", a)); //oob
	assert(mbsg.makeShips2("07v", a)); //vert S at 0,7
	a++;
	assert(mbsg.makeShips2("90v", a)); //vert D at 9,0


	assert(mbsg.getP1Ships().size() == 5);
	assert(mbsg.getP2Ships().size() == 5);


	assert(mbsg.moveShip("#8l2") == RESULT_INVALID); //bad input
	assert(mbsg.moveShip("8#l2") == RESULT_INVALID); //bad input
	assert(mbsg.moveShip("88o2") == RESULT_INVALID); //bad input
	assert(mbsg.moveShip("88l0") == RESULT_INVALID); //bad input
	assert(mbsg.moveShip("88l4") == RESULT_INVALID); //bad input


	assert(mbsg.moveShip("88l2") == RESULT_INVALID); //no ship there

	assert(mbsg.moveShip("00r1") == RESULT_INVALID); //vert cant move right
	assert(mbsg.moveShip("98l1") == RESULT_INVALID); //vert cant move left
	assert(mbsg.moveShip("80d1") == RESULT_INVALID); //hori cant move down
	assert(mbsg.moveShip("09u1") == RESULT_INVALID); //hori cant move up

	assert(mbsg.moveShip("00u1") == RESULT_INVALID); //illegal move off top
	assert(mbsg.moveShip("98d2") == RESULT_INVALID); //illegal move off bottom
	assert(mbsg.moveShip("80r1") == RESULT_INVALID); //illegal move off right
	assert(mbsg.moveShip("09l3") == RESULT_INVALID); //illegal move off left

	//p1 up and down
	assert(mbsg.moveShip("03d2") == RESULT_KEEP_PLAYING); //down two
	assert(mbsg.getBoard1().getGrid()[6][0] == 'A');
	assert(mbsg.getBoard1().getGrid()[0][0] == '-');
	assert(mbsg.getP1Ships().at(0).locations.at(4) == make_pair(6, 0));

	mbsg.setCurr(mbsg.getP1());
	assert(mbsg.moveShip("06u1") == RESULT_KEEP_PLAYING); //up 1
	assert(mbsg.getBoard1().getGrid()[1][0] == 'A');
	assert(mbsg.getBoard1().getGrid()[6][0] == '-');
	assert(mbsg.getP1Ships().at(0).locations.at(4) == make_pair(5, 0));

	mbsg.setCurr(mbsg.getP1());
	assert(mbsg.moveShip("02u1") == RESULT_KEEP_PLAYING); //up one to edge
	assert(mbsg.getBoard1().getGrid()[0][0] == 'A');
	assert(mbsg.getBoard1().getGrid()[6][0] == '-');
	assert(mbsg.getP1Ships().at(0).locations.at(4) == make_pair(4, 0));


	assert(mbsg.moveShip("88l2") == RESULT_INVALID); //no ship there

	assert(mbsg.moveShip("00d1") == RESULT_INVALID); //vert cant move right
	assert(mbsg.moveShip("89u1") == RESULT_INVALID); //vert cant move left
	assert(mbsg.moveShip("08r1") == RESULT_INVALID); //hori cant move down
	assert(mbsg.moveShip("90l1") == RESULT_INVALID); //hori cant move up

	assert(mbsg.moveShip("00l1") == RESULT_INVALID); //illegal move off top
	assert(mbsg.moveShip("89r2") == RESULT_INVALID); //illegal move off bottom
	assert(mbsg.moveShip("08d1") == RESULT_INVALID); //illegal move off right
	assert(mbsg.moveShip("90u3") == RESULT_INVALID); //illegal move off left


	assert(mbsg.moveShip("30r2") == RESULT_KEEP_PLAYING); //right two
	assert(mbsg.getBoard2().getGrid()[0][6] == 'A');
	assert(mbsg.getBoard2().getGrid()[0][0] == '-');
	assert(mbsg.getP2Ships().at(0).locations.at(4) == make_pair(0, 6));

	mbsg.setCurr(mbsg.getP2());
	assert(mbsg.moveShip("40l1") == RESULT_KEEP_PLAYING); //left 1
	assert(mbsg.getBoard2().getGrid()[0][1] == 'A');
	assert(mbsg.getBoard2().getGrid()[0][6] == '-');
	assert(mbsg.getP2Ships().at(0).locations.at(4) == make_pair(0, 5));

	mbsg.setCurr(mbsg.getP2());
	assert(mbsg.moveShip("20l1") == RESULT_KEEP_PLAYING); //left one to edge
	assert(mbsg.getBoard2().getGrid()[0][0] == 'A');
	assert(mbsg.getBoard2().getGrid()[0][6] == '-');
	assert(mbsg.getP2Ships().at(0).locations.at(4) == make_pair(0, 4));


	assert(mbsg.getP2Ships().at(0).locations.size() == 5);
	assert(mbsg.getP2Ships().at(0).hits.size() == 0);
	GameResult result = mbsg.attack_square(make_pair(0, 1)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP2Ships().at(0).locations.size() == 4);
	assert(mbsg.getP2Ships().at(0).hits.size() == 1);


	assert(mbsg.getP1Ships().at(0).locations.size() == 5);
	assert(mbsg.getP1Ships().at(0).hits.size() == 0);
	result = mbsg.attack_square(make_pair(1, 0)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP1Ships().at(0).locations.size() == 4);
	assert(mbsg.getP1Ships().at(0).hits.size() == 1);


	result = mbsg.attack_square(make_pair(0, 1)); // P1 HIT SAME
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP2Ships().at(0).locations.size() == 4);
	assert(mbsg.getP2Ships().at(0).hits.size() == 1);


	result = mbsg.attack_square(make_pair(1, 0)); // P2 HIT SAME
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP1Ships().at(0).locations.size() == 4);
	assert(mbsg.getP1Ships().at(0).hits.size() == 1);


	result = mbsg.attack_square(make_pair(5, 5)); // P1 MISS
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP2Ships().at(0).locations.size() == 4);
	assert(mbsg.getP2Ships().at(0).hits.size() == 1);


	result = mbsg.attack_square(make_pair(5, 5)); // P2 MISS
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP1Ships().at(0).locations.size() == 4);
	assert(mbsg.getP1Ships().at(0).hits.size() == 1);


	result = mbsg.attack_square(make_pair(0, 10)); // oob
	assert(result == RESULT_INVALID);


	result = mbsg.attack_square(make_pair(10, 0)); // oob
	assert(result == RESULT_INVALID);


	result = mbsg.attack_square(make_pair(-1, 0)); // oob
	assert(result == RESULT_INVALID);


	result = mbsg.attack_square(make_pair(0, -1)); // oob
	assert(result == RESULT_INVALID);


	assert(mbsg.getP2Ships().at(4).locations.size() == 2);
	assert(mbsg.getP2Ships().at(4).hits.size() == 0);
	result = mbsg.attack_square(make_pair(1, 9)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP2Ships().at(4).locations.size() == 1);
	assert(mbsg.getP2Ships().at(4).hits.size() == 1);


	assert(mbsg.getP1Ships().at(4).locations.size() == 2);
	assert(mbsg.getP1Ships().at(4).hits.size() == 0);
	result = mbsg.attack_square(make_pair(9, 1)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP1Ships().at(4).locations.size() == 1);
	assert(mbsg.getP1Ships().at(4).hits.size() == 1);


	result = mbsg.attack_square(make_pair(0, 9)); // P1 HIT, SUNK D
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP2Ships().size() == 4);


	result = mbsg.attack_square(make_pair(9, 0)); // P2 HIT, SUNK D
	assert(result == RESULT_KEEP_PLAYING);
	assert(mbsg.getP1Ships().size() == 4);


	//quickly erases three ships
	for (int b = 0; b < 3; b++) {
		mbsg.p1Ships.erase(mbsg.p1Ships.begin());
	}


	assert(mbsg.getP1Ships().size() == 1);
	mbsg.setCurr(mbsg.getP2());
	result = mbsg.attack_square(make_pair(0, 9)); // HIT
	assert(result == RESULT_KEEP_PLAYING);
	mbsg.setCurr(mbsg.getP2());
	result = mbsg.attack_square(make_pair(0, 7)); // HIT
	assert(result == RESULT_KEEP_PLAYING);
	mbsg.setCurr(mbsg.getP2());
	result = mbsg.attack_square(make_pair(0, 8)); // P2 WINS
	assert(result == RESULT_PLAYER2_WINS);


	cout << "\nPASSED GAME 2" << endl;
}


int main(void) {
	cout << "\nRunning Battleship Tests...\n" << endl;
	game1();
	game2();
	cout << "\n...All Tests Passed!\n" << endl;
	return 0;
}
