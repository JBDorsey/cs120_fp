#include "checkers.h"

//Initializes the checker game
CheckersGame::CheckersGame() {
    setP1(Player("PLAYER 1", 'O'));
    setP2(Player("PLAYER 2", 'X'));
    setCurr(getP1());
    initCheckers();
    popBoard();
    printBoard();
}


//Destructor for CheckersGame
CheckersGame::~CheckersGame() {
	for(int i = 0; i < 8; i++) {
		delete[] getBoard()[i];
	}
	delete[] getBoard();

    for(auto it : p1Checkers) {
		delete it.second;
	}
    p1Checkers.clear();

	for(auto it : p2Checkers) {
		delete it.second;
	}
    p2Checkers.clear();

    for(auto it : jumpable) {
		delete it;
	}
    jumpable.clear();




}

//Populates board of checkers
//NOTE: Even blank spaces are checkers
//All checkers contain an isSpace bool
void CheckersGame::popBoard() {
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP2().getSymbol(), false, make_pair(0, 1 + 2*i));
        getBoard()[0][1 + 2*i] = newChecker;
        p2Checkers[make_pair(0,1+2*i)] = &getBoard()[0][1+2*i];
    }
    for(int i = 0; i < 4; i++) {
        getBoard()[0][2*i] = Checker('-', true, make_pair(0, 2*i));
    }
    for(int i = 0; i < 4; i++) {
        getBoard()[1][1 + 2*i] = Checker('-', true, make_pair(1, 1 + 2*i));
    }
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP2().getSymbol(), false, make_pair(1, 2*i));
        getBoard()[1][2*i] = newChecker;
        p2Checkers[make_pair(1,2*i)] = &getBoard()[1][2*i];
    }
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP2().getSymbol(), false, make_pair(2, 1 + 2*i));
        getBoard()[2][1 + 2*i] = newChecker;
        p2Checkers[make_pair(2,1+2*i)] = &getBoard()[2][1+2*i];
    }
    for(int i = 0; i < 4; i++) {
        getBoard()[2][2*i] = Checker('-', true, make_pair(2, 2*i));
    }
    for(int i = 3; i < 5; i++) {
        for(int j = 0; j < 8; j++) {
            getBoard()[i][j] = Checker('-', true, make_pair(i, j));
        }
    }
    for(int i = 0; i < 4; i++) {
        getBoard()[5][1 + 2*i] = Checker('-', true, make_pair(5, 1 + 2*i));
    }
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP1().getSymbol(), false, make_pair(5, 2*i));
        getBoard()[5][2*i] = newChecker;
        p1Checkers[make_pair(5,2*i)] = &getBoard()[5][2*i];
    }
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP1().getSymbol(), false, make_pair(6, 1 + 2*i));
        getBoard()[6][1 + 2*i] = newChecker;
        p1Checkers[make_pair(6,1+2*i)] = &getBoard()[6][1+2*i];


    }
    for(int i = 0; i < 4; i++) {
        getBoard()[6][2*i] = Checker('-', true, make_pair(6, 2*i));
    }
    for(int i = 0; i < 4; i++) {
        getBoard()[7][1 + 2*i] = Checker('-', true, make_pair(7, 1 + 2*i));
    }
    for(int i = 0; i < 4; i++) {
        Checker newChecker(getP1().getSymbol(), false, make_pair(7, 2*i));
        getBoard()[7][2*i] = newChecker;
        p1Checkers[make_pair(7,2*i)] = &getBoard()[7][2*i];


    }
}

//Prints all board symbols
void CheckersGame::printBoard() {
    for (int a = 0; a < 8; a++) {
        for (int b = 0; b < 8; b++) {
            cout << getBoard()[a][b].symbol << " ";
        }
        cout << endl;
    }
    cout << endl;
    cout << endl;
}

//Creates all checker objects for the board
void CheckersGame::initCheckers() {
    checkersBoard = new Checker*[8];
    for (int a = 0; a < 8; a++) {
        checkersBoard[a] = new Checker[8];
    }

}


/*
    Checks to see if a checker is at TL of a given
    checker. Returns true if so and also returns a
    checker. If true returns the checkers adjacent
    otherwise returns the original checker.
*/
pair<bool, Checker> CheckersGame::checkUL(Checker checker) {
    pair<bool, Checker> result;
    if(checker.location.second == 0) {
        return make_pair(false, checker);
    }
    if(checker.location.first-1 < 0 || checker.location.second < 0) {
        return make_pair(false, checker);
    }
    Coord ul = make_pair(checker.location.first-1, checker.location.second-1);
    if(getBoard()[ul.first][ul.second].space == false) {
        result.first = false;
    } else {
        result.first = true;
    }
    result.second = getBoard()[ul.first][ul.second];
    return result;
}

/*
    Checks to see if a checker is at UR of a given
    checker. Returns true if so and also returns a
    checker. If true returns the checkers adjacent
    otherwise returns the original checker.
*/
pair<bool, Checker> CheckersGame::checkUR(Checker checker) {
    pair<bool, Checker> result;
    if(checker.location.second == 7) {
        return make_pair(false, checker);
    }
    if(checker.location.first-1 < 0 || checker.location.second+1 > 7) {
        return make_pair(false, checker);
    }
    Coord ur = make_pair(checker.location.first-1, checker.location.second+1);
    if(!getBoard()[ur.first][ur.second].space) {
        result.first = false;
    } else {
        result.first = true;
    }
    result.second = getBoard()[ur.first][ur.second];
    return result;
}

/*
    Checks to see if a checker is at BL of a given
    checker. Returns true if so and also returns a
    checker. If true returns the checkers adjacent
    otherwise returns the original checker.
*/
pair<bool, Checker> CheckersGame::checkBL(Checker checker) {
    pair<bool, Checker> result;
    if(checker.location.first+1 > 7 || checker.location.second-1 < 0) {
        return make_pair(false, checker);
    }
    Coord bl = make_pair(checker.location.first+1, checker.location.second-1);
    if(getBoard()[bl.first][bl.second].space == false) {
        result.first = false;
    } else {
        result.first = true;
    }
    result.second = getBoard()[bl.first][bl.second];
    return result;
}

/*
    Checks to see if a checker is at BR of a given
    checker. Returns true if so and also returns a
    checker. If true returns the checkers adjacent
    otherwise returns the original checker.
*/
pair<bool, Checker> CheckersGame::checkBR(Checker checker) {
    pair<bool, Checker> result;
    if(checker.location.first == 0) {
        return make_pair(false, checker);
    }
    if(checker.location.first+1 > 7 || checker.location.second+1 > 7) {
        return make_pair(false, checker);
    }
    Coord br = make_pair(checker.location.first+1, checker.location.second+1);
    if(getBoard()[br.first][br.second].space == false) {
        result.first = false;
    } else {
        result.first = true;
    }
    result.second = getBoard()[br.first][br.second];
    return result;
}

/*
    Populates a given checkers list of available
    jumps with all jumps possible in the 'B' direction
    on the board for a normal checker.
*/
void CheckersGame::downJumps(Checker &checker) {
    if((!checkBL(checker).first)
        && (checkBL(checker).second.symbol == 'O' || checkBL(checker).second.symbol == 'Q')
        && (checkBL(checkBL(checker).second).first)) {
            checker.jumps.push_back(checkBL(checker).second.location);
        }
    if((!checkBR(checker).first)
        && (checkBR(checker).second.symbol == 'O' || checkBR(checker).second.symbol == 'Q')
        && (checkBR(checkBR(checker).second).first)) {
            checker.jumps.push_back(checkBR(checker).second.location);
        }

}

/*
    Populates a given checkers list of available
    jumps with all jumps possible in the 'U' direction
    on the board for a normal checker.
*/
void CheckersGame::upJumps(Checker &checker) {
    if((!checkUL(checker).first)
        && (checkUL(checker).second.symbol == 'X' || checkUL(checker).second.symbol == 'K')
        && (checkUL(checkUL(checker).second).first)) {
            checker.jumps.push_back(checkUL(checker).second.location);
        }
    if((!checkUR(checker).first)
        && (checkUR(checker).second.symbol == 'X' || checkUR(checker).second.symbol == 'K')
        && (checkUR(checkUR(checker).second).first)) {
            checker.jumps.push_back(checkUR(checker).second.location);
        }
}

/*
    Populates a given checkers list of available
    jumps with all jumps possible in the 'U' direction
    on the board for a King checker.
*/
void CheckersGame::upKJumps(Checker &checker) {
    if(checker.symbol == 'K') {
        if((!checkUL(checker).first)
            && (checkUL(checker).second.symbol == 'O' || checkUL(checker).second.symbol == 'Q')
            && (checkUL(checkUL(checker).second).first)) {
                checker.jumps.push_back(checkUL(checker).second.location);
            }
        if((!checkUR(checker).first)
            && (checkUR(checker).second.symbol == 'O' || checkUR(checker).second.symbol == 'Q')
            && (checkUR(checkUR(checker).second).first)) {
                checker.jumps.push_back(checkUR(checker).second.location);
            }
    } else {
        upJumps(checker);
    }
}

/*
    Populates a given checkers list of available
    jumps with all jumps possible in the 'B' direction
    on the board for a King checker.
*/
void CheckersGame::downKJumps(Checker &checker) {
    if(checker.symbol == 'K') {
        downJumps(checker);
    } else {
        if((checkBL(checker).first == false)
            && (checkBL(checker).second.symbol == 'X' || checkBL(checker).second.symbol == 'K')
            && (checkBL(checkBL(checker).second).first == true)) {
                checker.jumps.push_back(checkBL(checker).second.location);
            }
        if((checkBR(checker).first == false)
            && (checkBR(checker).second.symbol == 'X' || checkBR(checker).second.symbol == 'K')
            && (checkBR(checkBR(checker).second).first == true)) {
                checker.jumps.push_back(checkBR(checker).second.location);
            }

    }
}

/*
    Calls the above explained jump checks.
    Returns true if the given checker has
    any jumps.
*/
bool CheckersGame::jumpAt(Checker &checker) {
    checker.jumps.clear();
    if(checker.symbol == 'X') {
        //If a regular X piece
        downJumps(checker);
    } else if(checker.symbol == 'O') {
        //If a regular O piece
        upJumps(checker);
    } else if(checker.symbol == 'K' || checker.symbol == 'Q') {
        //If a king of either team
        downKJumps(checker);
        upKJumps(checker);
    }

    if(checker.jumps.empty()) {
        return false;
    } else {
        return true;
    }
}

/*
    After each turn this is called and
    each players list of still alive checkers
    is updated.
*/
void CheckersGame::popLists() {
    p1Checkers.clear();
    p2Checkers.clear();
    for (unsigned int a = 0; a < 8; a++) {
        for (unsigned int b = 0; b < 8; b++) {
            char cur = getBoard()[a][b].symbol;
            if(cur == 'X' || cur == 'K') {
                p2Checkers[make_pair(a, b)] = &getBoard()[a][b];
            } else if(cur == 'O' || cur == 'Q') {
                p1Checkers[make_pair(a, b)] = &getBoard()[a][b];
            }
        }
    }
}

/*
    Called at the beginning of each turn.
    Populates the list of checkers
    "jumpable" and if the player has any
    checkers with jumps (jumpable gets
    added too) it returns true. Helps
    make sure the player is only jumping
    if he has a jump. Uses jumpAt for each
    checker to easily see if we need to
    push_back onto jumpable.
*/
bool CheckersGame::checkIfJumps() {
    jumpable.clear();
    if(getCurr().getSymbol() == 'X' || getCurr().getSymbol() == 'K') {
        for (unsigned int a = 0; a < 8; a++) {
            for (unsigned int b = 0; b < 8; b++) {
                if(getBoard()[a][b].symbol == 'X' || getBoard()[a][b].symbol == 'K') {
                    if (jumpAt(getBoard()[a][b])) {
                        jumpable.push_back(&getBoard()[a][b]);
                    }
                }
        }
    }
    } else {
        for (unsigned int a = 0; a < 8; a++) {
            for (unsigned int b = 0; b < 8; b++) {
                if(getBoard()[a][b].symbol == 'O' || getBoard()[a][b].symbol == 'Q') {
                    if (jumpAt(getBoard()[a][b])) {
                        jumpable.push_back(&getBoard()[a][b]);
                    }
            }
        }
        }
    }
    if(jumpable.empty()) {
        return false;
    }
    return true;
}

/*
    Handles each move given to the game.
    Implements the logic of checkIfJumps
    and doesn't let a player move he has a
    jump. Doesn't let the player into the
    moveChecker (which physically moves
    checkers) unless proper conditions are
    met.
*/
GameResult CheckersGame::moveDriver(string str) {
    int check = -1;
    popLists();
    //If player 1 is moving (for specific symbol checking)
    if (checkIfJumps()) {
        //If player has a jump, iterate over jumpable
        //and make sure the players move call is a
        //jump in jumpable. Otherwise return invalid.
        Coord loc = make_pair((int)str[1]-'0', (int)str[0]-'0');
        for(auto it = jumpable.begin(); it != jumpable.end(); it++) {
            if((*it)->location.first == loc.first && (*it)->location.second == loc.second) {
                check = 0;
                return moveChecker(str);
            }
        }
        if (check != -1) {
            return RESULT_INVALID;
        }
    } else {
        //If player has no jumps, move normally
        return moveChecker(str);
    }
    return RESULT_INVALID;
}

/*
    Normal movement function called by moveDriver.
*/
GameResult CheckersGame::moveChecker(string str) {
    Coord loc = make_pair((int)str[1]-'0', (int)str[0]-'0');
    string dest = str.substr(2);
    Checker &curr(getBoard()[loc.first][loc.second]);
    if(!jumpAt(curr)){
        /*If there's not a jump at the given checker
          just call directional move statements
          normally.
        */
        if(dest.compare("tl") == 0) {
            return moveTL(curr, loc);
        } else if(dest.compare("tr") == 0) {
            return moveTR(curr, loc);
        } else if (dest.compare("bl") == 0) {
            return moveBL(curr, loc);
        } else if (dest.compare("br") == 0) {
            return moveBR(curr, loc);
        } else {
            return RESULT_INVALID;
        }
    } else {
        /*If there's a jump at the given checker
          check if the move your making is that jump
          with contains method.
        */
        if(dest.compare("tl") == 0) {
            if(!contains(curr, getBoard()[loc.first-1][loc.second-1])) {
                return RESULT_INVALID;
            } else {
                return moveTL(curr, loc);
            }
        }else if(dest.compare("tr") == 0) {
            if(!contains(curr, getBoard()[loc.first-1][loc.second+1])) {
                return RESULT_INVALID;
            } else {
                return moveTR(curr, loc);
            }
        } else if(dest.compare("bl") == 0) {
            if(!contains(curr, getBoard()[loc.first+1][loc.second-1])) {
                return RESULT_INVALID;
            } else {
                return moveBL(curr, loc);
            }
        } else if(dest.compare("br") == 0) {
            if(!contains(curr, getBoard()[loc.first+1][loc.second+1])) {
                return RESULT_INVALID;
            } else {
                return moveBR(curr, loc);
            }
        } else {
            //Else jump is invalid
            return RESULT_INVALID;
        }
    }
    //Needed for compiler
    return RESULT_INVALID;
}

/*
    Checks if a players checker
    list is empty (i.e other
    person wins).
*/
GameResult CheckersGame::checkWin() {
    if(p1Checkers.empty()) {
        return RESULT_PLAYER2_WINS;
    } else if(p2Checkers.empty()) {
        return RESULT_PLAYER1_WINS;
    } else {
        return RESULT_KEEP_PLAYING;
    }
}

/*
    Returns if the checker comp exists in
    orig's list of checker jumps.
*/
bool CheckersGame::contains(Checker &orig, Checker &comp) {
    for(auto it = orig.jumps.begin(); it != orig.jumps.end(); it++) {
        if(it->first == comp.location.first && it->second == comp.location.second) {
            return true;
        }
    }
    return false;
}

/*
    Checks if a given checker needs to
    be turned into a King based on its
    position.
*/
void CheckersGame::checkKing(Checker &checker) {
    char player = checker.symbol;
    switch(player) {
        case 'X':
            if(checker.location.first == 7) {
                checker.symbol = 'K';
                checker.king = true;
                cout << "CROWNED\n" << endl;
            }
        case 'O':
            if(checker.location.first == 0) {
                checker.symbol = 'Q';
                checker.king = true;
                cout << "CROWNED\n" << endl;
            }
    }
}

/*
    Swaps the position of two
    checkers. Can swap and blank
    checker with a real checker.
*/
void CheckersGame::swapCheckers(Checker &orig, Checker &dest) {
    Coord oldLoc = orig.location;
    Coord newLoc = dest.location;
    Checker temp = orig;
    Checker temp2 = dest;
    temp.location = newLoc;
    temp2.location = oldLoc;
    dest = temp;
    orig = temp2;
}

/*
    Changes who's turn it is by
    setting the curr player variable
    to whoevers turn it needs to be
*/
void CheckersGame::swapPlayer() {
    if (getCurr().equals(getP1())) {
        setCurr(getP2());
    } else {
        setCurr(getP1());
    }
}

/*
    Actual content of moving a piece
    TL.
*/
GameResult CheckersGame::moveTL(Checker &curr, Coord loc) {
    Checker &move = getBoard()[loc.first - 1][loc.second - 1];
    //Checks if the symbol of the piece we're trying
    //to move belongs to the current player.
    if(getCurr().getSymbol() == 'X') {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'K')) {
            return RESULT_INVALID;
        }
    } else {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'Q')) {
            return RESULT_INVALID;
        }
    }
    if(checkUL(curr).first) {
        //Just normal move
        Coord oldLoc = curr.location;
        Coord newLoc = move.location;
        swapCheckers(curr, move);
        swapPlayer();
        //After move check king
        checkKing(getBoard()[loc.first-1][loc.second-1]);
        popLists();
        if(checkWin() != RESULT_KEEP_PLAYING) {
            return checkWin();
        }
        return RESULT_KEEP_PLAYING;

    } else {
        if(contains(curr, move)) {
            //Need to jump over move
            cout << "JUMPED\n" << endl;
            move = Checker('-', true, move.location);
            Coord oldLoc = curr.location;
            Coord middle = move.location;
            swapCheckers(curr, getBoard()[loc.first-2][loc.second-2]);
            //After move check king
            checkKing(getBoard()[loc.first-2][loc.second-2]);
            //Checks if there's a double jump available
            if(!jumpAt(getBoard()[loc.first-2][loc.second-2])) {
                //If not end turn
                swapPlayer();
                popLists();
                if(checkWin() != RESULT_KEEP_PLAYING) {
                    return checkWin();
                }
                return RESULT_KEEP_PLAYING;
            } else {
                //Otherwise stay on player's turn
                return RESULT_DOUBLE_JUMP;
            }
        } else {
            //INVALID JUMP
            return RESULT_INVALID;
        }
    }
}

GameResult CheckersGame::moveTR(Checker &curr, Coord loc) {
    Checker &move = getBoard()[loc.first - 1][loc.second + 1];
    //Checks if the symbol of the piece we're trying
    //to move belongs to the current player.
    if(getCurr().getSymbol() == 'X') {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'K')) {
            return RESULT_INVALID;
        }
    } else {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'Q')) {
            return RESULT_INVALID;
        }
    }
    if(checkUR(curr).first) {
        //Just normal move
        swapCheckers(curr, move);
        swapPlayer();
        checkKing(getBoard()[loc.first-1][loc.second+1]);
        popLists();
        if(checkWin() != RESULT_KEEP_PLAYING) {
            return checkWin();
        }
        return RESULT_KEEP_PLAYING;

    } else {
        if(contains(curr, move)) {
            //Curr must jump over move
            cout << "JUMPED\n" << endl;
            move = Checker('-', true, move.location);
            Coord oldLoc = curr.location;
            Coord middle = move.location;
            swapCheckers(curr, getBoard()[loc.first-2][loc.second+2]);
            checkKing(getBoard()[loc.first-2][loc.second+2]);
            //Checks if a double jump is available
            if(!jumpAt(getBoard()[loc.first-2][loc.second+2])) {
                //If not end turn
                swapPlayer();
                popLists();
                if(checkWin() != RESULT_KEEP_PLAYING) {
                    return checkWin();
                }
                return RESULT_KEEP_PLAYING;
            } else {
                //If so stay on player's turn
                return RESULT_DOUBLE_JUMP;
            }
        } else {
            //INVALID JUMP
            return RESULT_INVALID;
        }
    }
}

GameResult CheckersGame::moveBL(Checker &curr, Coord loc) {
    Checker &move = getBoard()[loc.first + 1][loc.second - 1];
    //Checks if the symbol of the piece we're trying
    //to move belongs to the current player.
    if(getCurr().getSymbol() == 'X') {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'K')) {
            return RESULT_INVALID;
        }
    } else {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'Q')) {
            return RESULT_INVALID;
        }
    }
    if(checkBL(curr).first) {
        //Just normal move
        swapCheckers(curr, move);
        swapPlayer();
        checkKing(getBoard()[loc.first+1][loc.second-1]);
        popLists();
        if(checkWin() != RESULT_KEEP_PLAYING) {
            return checkWin();
        }
        return RESULT_KEEP_PLAYING;

    } else {
        if(contains(curr, move)) {
            //curr must jump move
            cout << "JUMPED\n" << endl;
            move = Checker('-', true, move.location);
            Coord oldLoc = curr.location;
            Coord middle = move.location;
            swapCheckers(curr, getBoard()[loc.first+2][loc.second-2]);
            checkKing(getBoard()[loc.first+2][loc.second-2]);
            //Check if there's a double jump
            if(!jumpAt(getBoard()[loc.first+2][loc.second-2])) {
                //If not end turn
                swapPlayer();
                popLists();
                if(checkWin() != RESULT_KEEP_PLAYING) {
                    return checkWin();
                }
                return RESULT_KEEP_PLAYING;
            } else {
                //If so stay on player's turn
                return RESULT_DOUBLE_JUMP;
            }
        } else {
            //INVALID JUMP
            return RESULT_INVALID;
        }
    }
}

GameResult CheckersGame::moveBR(Checker &curr, Coord loc) {
    Checker &move = getBoard()[loc.first + 1][loc.second + 1];
    //Checks if the symbol of the piece we're trying
    //to move belongs to the current player.
    if(getCurr().getSymbol() == 'X') {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'K')) {
            return RESULT_INVALID;
        }
    } else {
        if(curr.symbol != getCurr().getSymbol() && (curr.symbol != 'Q')) {
            return RESULT_INVALID;
        }
    }
    if(checkBR(curr).first == true) {
        //Just normal move
        swapCheckers(curr, move);
        swapPlayer();
        checkKing(getBoard()[loc.first+1][loc.second+1]);
        popLists();
        if(checkWin() != RESULT_KEEP_PLAYING) {
            return checkWin();
        }
        return RESULT_KEEP_PLAYING;

    } else {
        if(contains(curr, move)) {
            //curr must jump move
            cout << "JUMPED\n" << endl;
            move = Checker('-', true, move.location);
            Coord oldLoc = curr.location;
            Coord middle = move.location;
            swapCheckers(curr, getBoard()[loc.first+2][loc.second+2]);
            checkKing(getBoard()[loc.first+2][loc.second+2]);
            //Check if double jump exists
            if(!jumpAt(getBoard()[loc.first+2][loc.second+2])) {
                //If not end turn
                swapPlayer();
                return RESULT_KEEP_PLAYING;
                popLists();
                if(checkWin() != RESULT_KEEP_PLAYING) {
                    return checkWin();
                }
            } else {
                //If so stay on player's turn
                return RESULT_DOUBLE_JUMP;
            }
        } else {
            //INVALID JUMP
            return RESULT_INVALID;
        }
    }
}
