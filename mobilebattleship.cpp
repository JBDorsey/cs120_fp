/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * Final Project
 * 4/29/16
 * battleship.cpp
 */

#include "battleship.h"
#include "mobilebattleship.h"


/**
 * Standard constructor for mobile bs
 */
MobileBattleshipGame::MobileBattleshipGame() {
}


/**
 * Parses the string to move a ship and calls funcitons accordingly.
 */
GameResult MobileBattleshipGame::moveShip(string str) {
	//Checks if the numbers are valid.
	if (!isdigit(str[0]) || !isdigit(str[1])
			|| str[3] - 48 < 1 || str[3] - 48 > 3) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if a ship exixsts at that coordinate
	Coord coord = make_pair((int)str[1] - 48, (int)str[0] - 48);
	int loc = findShip(coord);
	if (loc == -1) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	char dir = str[2];
	int dist = (int)str[3] - 48;

	return callMove(loc, dist, dir);
}


/**
 * Simply calls the corresponding function
 */
GameResult MobileBattleshipGame::callMove(int loc, int dist, char dir) {
	if (getCurr().equals(getP1())) {
		switch (dir) {
			case 'u':
				return moveUp1(loc, dist);
				break;
			case 'd':
				return moveDown1(loc, dist);
				break;
			case 'l':
				return moveLeft1(loc, dist);
				break;
			case 'r':
				return moveRight1(loc, dist);
				break;
			default:
				cout << "INVALID MOVE" << endl;
				return RESULT_INVALID;
				break;
		}
	} else {
		switch (dir) {
			case 'u':
				return moveUp2(loc, dist);
				break;
			case 'd':
				return moveDown2(loc, dist);
				break;
			case 'l':
				return moveLeft2(loc, dist);
				break;
			case 'r':
				return moveRight2(loc, dist);
				break;
			default:
				cout << "INVALID MOVE" << endl;
				return RESULT_INVALID;
				break;
		}
	}
}


/**
 * Searches through the intended players ships locations and hits and returns the
 * of the desired ship, or -1 if there is no ship at that coordinate.
 */
int MobileBattleshipGame::findShip(Coord coord) {
	if (getCurr().equals(getP1())) {
		for (unsigned int a = 0; a < p1Ships.size(); a++) {
			//checks p1 ship locations
			for (unsigned int b = 0; b < p1Ships.at(a).locations.size(); b++) {
				if(p1Ships.at(a).locations.at(b).first == coord.first
						&& p1Ships.at(a).locations.at(b).second == coord.second) {
					return a;
				}
			}
			//checks p1 ship locations that have been hit
			for (unsigned int b = 0; b < p1Ships.at(a).hits.size(); b++) {
				if(p1Ships.at(a).hits.at(b).first == coord.first
						&& p1Ships.at(a).hits.at(b).second == coord.second) {
					return a;
				}
			}
		}
	} else {
		for (unsigned int a = 0; a < p2Ships.size(); a++) {
			//checks p2 ship locations
			for (unsigned int b = 0; b < p2Ships.at(a).locations.size(); b++) {
				if(p2Ships.at(a).locations.at(b).first == coord.first
						&& p2Ships.at(a).locations.at(b).second == coord.second) {
					return a;
				}
			}
			//checks p2 ship locations that have been hit
			for (unsigned int b = 0; b < p2Ships.at(a).hits.size(); b++) {
				if(p2Ships.at(a).hits.at(b).first == coord.first
						&& p2Ships.at(a).hits.at(b).second == coord.second) {
					return a;
				}
			}
		}
	}
	return -1;
}

/**
 * Takes care of checking to see if a move up is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveUp1(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p1Ships.at(loc).orientation != 'v') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p1Ships.at(loc).first;
	bottom = p1Ships.at(loc).last;

	//if attempting to move off the board
	if (top.first - dist < 0) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard1().getGrid()[top.first - a][top.second] != '-'
				&& getBoard1().getGrid()[top.first - a][top.second] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = top.first - dist; b <= bottom.first - dist; b++) {
		getBoard1().getGrid()[b][top.second]
			= getBoard1().getGrid()[b + dist][top.second];
	}
	for (int c = bottom.first - dist + 1; c <= bottom.first; c++) {
		getBoard1().getGrid()[c][top.second] = '-';
	}

	//This call changes the location of the ship and its hits
	p1Ships.at(loc).up(dist);

	setCurr(getP2());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move up is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveUp2(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p2Ships.at(loc).orientation != 'v') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p2Ships.at(loc).first;
	bottom = p2Ships.at(loc).last;

	//if attempting to move off the board
	if (top.first - dist < 0) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard2().getGrid()[top.first - a][top.second] != '-'
				&& getBoard2().getGrid()[top.first - a][top.second] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = top.first - dist; b <= bottom.first - dist; b++) {
		getBoard2().getGrid()[b][top.second]
			= getBoard2().getGrid()[b + dist][top.second];
	}
	for (int c = bottom.first - dist + 1; c <= bottom.first; c++) {
		getBoard2().getGrid()[c][top.second] = '-';
	}

	//This call changes the location of the ship and its hits
	p2Ships.at(loc).up(dist);

	setCurr(getP1());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move down is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveDown1(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p1Ships.at(loc).orientation != 'v') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p1Ships.at(loc).first;
	bottom = p1Ships.at(loc).last;

	//if attempting to move off the board
	if (bottom.first + dist > 9) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard1().getGrid()[bottom.first + a][top.second] != '-'
				&& getBoard1().getGrid()[bottom.first + a][top.second] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = bottom.first + dist; b >= top.first + dist; b--) {
		getBoard1().getGrid()[b][top.second]
			= getBoard1().getGrid()[b - dist][top.second];
	}
	for (int c = top.first + dist - 1; c >= top.first; c--) {
		getBoard1().getGrid()[c][top.second] = '-';
	}

	//This call changes the location of the ship and its hits
	p1Ships.at(loc).down(dist);

	setCurr(getP2());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move down is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveDown2(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p2Ships.at(loc).orientation != 'v') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p2Ships.at(loc).first;
	bottom = p2Ships.at(loc).last;

	//if attempting to move off the board
	if (bottom.first + dist > 9) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard2().getGrid()[bottom.first + a][top.second] != '-'
				&& getBoard2().getGrid()[bottom.first + a][top.second] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = bottom.first + dist; b >= top.first + dist; b--) {
		getBoard2().getGrid()[b][top.second]
			= getBoard2().getGrid()[b - dist][top.second];
	}
	for (int c = top.first + dist - 1; c >= top.first; c--) {
		getBoard2().getGrid()[c][top.second] = '-';
	}

	//This call changes the location of the ship and its hits
	p2Ships.at(loc).down(dist);

	setCurr(getP1());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move left is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveLeft1(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p1Ships.at(loc).orientation != 'h') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p1Ships.at(loc).first;
	bottom = p1Ships.at(loc).last;

	//if attempting to move off the board
	if (top.second - dist < 0) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard1().getGrid()[top.first][top.second - a] != '-'
				&& getBoard1().getGrid()[top.first][top.second - a] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = top.second - dist; b <= bottom.second - dist; b++) {
		getBoard1().getGrid()[top.first][b]
			= getBoard1().getGrid()[top.first][b + dist];
	}
	for (int c = bottom.second - dist + 1; c <= bottom.second; c++) {
		getBoard1().getGrid()[top.first][c] = '-';
	}

	//This call changes the location of the ship and its hits
	p1Ships.at(loc).left(dist);

	setCurr(getP2());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move left is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveLeft2(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p2Ships.at(loc).orientation != 'h') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p2Ships.at(loc).first;
	bottom = p2Ships.at(loc).last;

	//if attempting to move off the board
	if (top.second - dist < 0) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard2().getGrid()[top.first][top.second - a] != '-'
				&& getBoard2().getGrid()[top.first][top.second - a] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = top.second - dist; b <= bottom.second - dist; b++) {
		getBoard2().getGrid()[top.first][b]
			= getBoard2().getGrid()[top.first][b + dist];
	}
	for (int c = bottom.second - dist + 1; c <= bottom.second; c++) {
		getBoard2().getGrid()[top.first][c] = '-';
	}

	//This call changes the location of the ship and its hits
	p2Ships.at(loc).left(dist);

	setCurr(getP1());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move right is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveRight1(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p1Ships.at(loc).orientation != 'h') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p1Ships.at(loc).first;
	bottom = p1Ships.at(loc).last;

	//if attempting to move off the board
	if (bottom.second + dist > 9) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard1().getGrid()[top.first][bottom.second + a] != '-'
				&& getBoard1().getGrid()[top.first][bottom.second + a] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = bottom.second + dist; b >= top.second + dist; b--) {
		getBoard1().getGrid()[top.first][b]
			= getBoard1().getGrid()[top.first][b - dist];
	}
	for (int c = top.second + dist - 1; c >= top.second; c--) {
		getBoard1().getGrid()[top.first][c] = '-';
	}

	//This call changes the location of the ship and its hits
	p1Ships.at(loc).right(dist);

	setCurr(getP2());
	return RESULT_KEEP_PLAYING;
}


/**
 * Takes care of checking to see if a move right is valid and alters the board if it is.
 */
GameResult MobileBattleshipGame::moveRight2(int loc, int dist) {
	Coord top;
	Coord bottom;

	//if not orented in direction of movement
	if (p2Ships.at(loc).orientation != 'h') {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	top = p2Ships.at(loc).first;
	bottom = p2Ships.at(loc).last;

	//if attempting to move off the board
	if (bottom.second + dist > 9) {
		cout << "INVALID MOVE" << endl;
		return RESULT_INVALID;
	}

	//Checks if any ships are in the way
	for (int a = 1; a <= dist; a++) {
		if (getBoard2().getGrid()[top.first][bottom.second + a] != '-'
				&& getBoard2().getGrid()[top.first][bottom.second + a] != 'M') {
			cout << "INVALID MOVE" << endl;
			return RESULT_INVALID;
		}
	}

	//Next two for loops actually change the board
	for (int b = bottom.second + dist; b >= top.second + dist; b--) {
		getBoard2().getGrid()[top.first][b]
			= getBoard2().getGrid()[top.first][b - dist];
	}
	for (int c = top.second + dist - 1; c >= top.second; c--) {
		getBoard2().getGrid()[top.first][c] = '-';
	}

	//This call changes the location of the ship and its hits
	p2Ships.at(loc).right(dist);

	setCurr(getP1());
	return RESULT_KEEP_PLAYING;
}
