#ifndef CHECKERS_H
#define CHECKERS_H

#include "game.h"
#include <map>

class Checker {
public:
	char symbol;
	bool king;
	bool space;
	std::vector<Coord> jumps;
	Coord location;

	Checker() {
		symbol = '-';
		king = false;
		space = true;
		location = make_pair<int, int>(-1, -1);
	}

	Checker(char player, bool isSpace, Coord loc) {
		symbol = player;
		king = false;
		space = isSpace;
		location = loc;
	}

	Checker(const Checker &copy) {
		symbol = copy.symbol;
		king = copy.king;
		space = copy.space;
		location = copy.location;
		jumps = copy.jumps;
	}



};


class CheckersGame : public Game {
public:
    CheckersGame();
	virtual ~CheckersGame();
    void popBoard();
    void printBoard();
	void popLists();
	std::map<Coord, Checker*> p1Checkers;
	std::map<Coord, Checker*> p2Checkers;
	std::vector<Checker*> jumpable;

	Checker** getBoard() {
    	return checkersBoard;
    }


	bool checkIfJumps();
	void upJumps(Checker &checker);
	void downJumps(Checker &checker);
	void upKJumps(Checker &checker);
	void downKJumps(Checker &checker);
	bool jumpAt(Checker &checker);

	pair<bool, Checker> checkUL(Checker checker);
	pair<bool, Checker> checkUR(Checker checker);
	pair<bool, Checker> checkBL(Checker checker);
	pair<bool, Checker> checkBR(Checker checker);

	GameResult moveDriver(string str);
	GameResult moveChecker(string str);
	GameResult moveTL(Checker &curr, Coord loc);
	GameResult moveTR(Checker &curr, Coord loc);
	GameResult moveBL(Checker &curr, Coord loc);
	GameResult moveBR(Checker &curr, Coord loc);

	GameResult checkWin();
	void swapCheckers(Checker &orig, Checker &dest);
	void swapPlayer();
	bool contains(Checker &orig, Checker &comp);
	void checkKing(Checker &checker);
	void p1Update(Coord oldLoc, Coord newLoc, Checker &curr);
	void p2Update(Coord oldLoc, Coord newLoc, Checker &curr);
	void p1Jump(Coord oldLoc, Coord newLoc, Coord middle, Checker &curr);
	void p2Jump(Coord oldLoc, Coord newLoc, Coord middle, Checker &curr);







private:
	Checker** checkersBoard;
	void initCheckers();
};

#endif
